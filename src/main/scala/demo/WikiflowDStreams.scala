package demo

import java.nio.charset.StandardCharsets
import java.util.Properties

import org.apache.spark.SparkConf
import org.apache.spark.sql.types.TimestampType
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.pubsub.{PubsubUtils, SparkGCPCredentials}
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.slf4j.{Logger, LoggerFactory}


object WikiflowDStreams {

  @transient lazy val logger: Logger = LoggerFactory.getLogger(WikiflowDStreams.getClass)
  val spark: SparkSession = SparkSession.builder.appName("WikiflowDStreams").getOrCreate()
  import spark.implicits._


  def main(args: Array[String]): Unit = {
    val Seq(projectID, subscr, topic, slidingInterval, dbuser, dbpass, dbip) = args.toSeq
    val ssc = new StreamingContext(spark.sparkContext, Seconds(slidingInterval.toInt))

    val rawStream: DStream[String] = PubsubUtils
      .createStream(
        ssc,
        projectID,
        Some(topic),
        subscr,
        SparkGCPCredentials.builder.build(),
        StorageLevel.MEMORY_AND_DISK_SER_2)
      .map(message => new String(message.getData(), StandardCharsets.UTF_8))


    val driverClassName = "org.postgresql.Driver"

    val connectionProperties = new Properties()
    connectionProperties.put("user", dbuser)
    connectionProperties.put("password", dbpass)
    connectionProperties.setProperty("Driver", driverClassName)
    val dbUrl = s"jdbc:postgresql://${dbip}/wikidata"

    rawStream.foreachRDD { rdd =>
      val df: DataFrame = spark.read.json(rdd.map(x => x))

      if (df.columns.contains("bot")) {
        logger.warn("Data structure is correct, processing the batch")
        val d = df.select($"type".as("event_type"),
                  $"timestamp".cast(TimestampType).alias("event_time"),
                  $"user",
                  $"minor",
                  $"length".getItem("old").as("len_old"),
                  $"length".getItem("new").as("len_new"),
                  $"wiki")
        d.coalesce(1)
          .write
          .mode("append")
          .jdbc(dbUrl, "edit_stream", connectionProperties)
      }
    }

//    val typeCalculator = rawStream.transform { rdd =>
//      val df = spark.read.json(rdd.map(x => x))
//
//      logger.warn("==="*10)
//      df.schema.foreach(field => logger.warn(s"Field data: ${field.name} with type ${field.dataType.simpleString}"))
//      logger.warn("==="*10)
//
//      if (df.columns.contains("bot")) { // check the input data for structure
//        logger.warn("Data structure is correct, processing the batch")
//        val typeCounter = df.filter(($"bot" === false) and ($"type" =!= "142")).groupBy($"type").count
//        typeCounter.rdd.map(row => (row(0).toString, row(1).asInstanceOf[Long]))
//      } else {
//        logger.warn("Bad input structure in input RDD")
//        spark.sparkContext.emptyRDD[(String, Long)] // missing structure error handling
//      }
//    }
//
//    val typeReducer = typeCalculator.reduceByKeyAndWindow(
//      (a: Long, b: Long) => a + b, // simple sum operation
//      Seconds(slidingInterval.toInt * 5), // window length (last X seconds of data)
//      Seconds(slidingInterval.toInt) // slide interval (how often no reduce)
//    )
//
//    typeReducer.foreachRDD { rdd =>
//      val typesCounterDF = rdd.toDF("type", "count").selectExpr("*", "CURRENT_TIMESTAMP() as load_dttm")
//      typesCounterDF.write.mode("append").jdbc(dbUrl, "types_count", connectionProperties)
//      logger.warn("New chunk of data produced to postgres")
//    }


    ssc.start()
    ssc.awaitTermination()
  }
}
